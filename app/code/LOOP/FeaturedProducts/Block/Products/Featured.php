<?php

namespace LOOP\FeaturedProducts\Block\Products;

use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Pricing\Price\FinalPrice;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\Pricing\Render;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Framework\Data\Helper\PostHelper;

class Featured extends Template implements BlockInterface
{
    const DEFAULT_PRODUCTS_COUNT = 4;

    /**
     * @var
     */
    private $productsCount;

    /**
     * @var HttpContext
     */
    private $httpContext;

    /**
     * @var Visibility
     */
    private $catalogProductVisibility;

    /**
     * @var CollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var Image
     */
    private $imageHelper;

    /**
     * @var Cart
     */
    private $cartHelper;

    /**
     * @var PostHelper
     */
    private $postHelper;

    /**
     * @param Context $context
     * @param CollectionFactory $productCollectionFactory
     * @param Visibility $catalogProductVisibility
     * @param HttpContext $httpContext
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $productCollectionFactory,
        Visibility $catalogProductVisibility,
        HttpContext $httpContext,
        PostHelper $postHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->productCollectionFactory = $productCollectionFactory;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->httpContext = $httpContext;
        $this->imageHelper = $context->getImageHelper();
        $this->cartHelper = $context->getCartHelper();
        $this->postHelper = $postHelper;
    }

    /**
     * @return Image|\Magento\Catalog\Helper\Image
     */
    public function imageHelperObj()
    {
        return $this->imageHelper;
    }

    /**
     * @return PostHelper
     */
    public function postHelper() {
        return $this->postHelper;
    }

    /**
     * @return mixed
     */
    public function getFeaturedProduct()
    {
        $limit = $this->getProductLimit();
        $collection = $this->productCollectionFactory->create();
        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());
        $collection->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->setPageSize($limit)
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('featured' , '1');

        return $collection;
    }

    /**
     * @return array|mixed|null
     */
    public function getProductLimit()
    {
        if ($this->getData('product.count') == '') {
            return self::DEFAULT_PRODUCTS_COUNT;
        }

        return $this->getData('product.count');
    }

    /**
     * @param $product
     * @param array $additional
     * @return string
     */
    public function getAddToCartUrl($product, $additional = [])
    {
        return $this->cartHelper->getAddUrl($product, $additional);
    }

    /**
     * @param Product $product
     * @param null $priceType
     * @param string $renderZone
     * @param array $arguments
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getProductPriceHtml(
        Product $product,
        $priceType = null,
        $renderZone = Render::ZONE_ITEM_LIST,
        array $arguments = []
    )
    {
        if (!isset($arguments['zone'])) {
            $arguments['zone'] = $renderZone;
        }
        $arguments['zone'] = isset($arguments['zone'])
            ? $arguments['zone']
            : $renderZone;
        $arguments['price_id'] = isset($arguments['price_id'])
            ? $arguments['price_id']
            : 'old-price-' . $product->getId() . '-' . $priceType;
        $arguments['include_container'] = isset($arguments['include_container'])
            ? $arguments['include_container']
            : true;
        $arguments['display_minimal_price'] = isset($arguments['display_minimal_price'])
            ? $arguments['display_minimal_price']
            : true;

        /** @var Render $priceRender */
        $priceRender = $this->getLayout()->getBlock('product.price.render.default');

        $price = '';
        if ($priceRender) {
            $price = $priceRender->render(
                FinalPrice::PRICE_CODE,
                $product,
                $arguments
            );
        }
        return $price;
    }
}
