<?php

namespace LOOP\Checkout\Plugin\Sales\Block\Adminhtml\Order\View\Info;

use Magento\Sales\Block\Adminhtml\Order\View\Info as Subject;
use LOOP\Checkout\Api\OrderCustomFieldsRepositoryInterface;

class CustomFields
{
    /**
     * @var OrderCustomFieldsRepositoryInterface
     */
    private $orderCustomFieldsRepository;

    /**
     * @param OrderCustomFieldsRepositoryInterface $orderCustomFieldsRepository
     */
    public function __construct(OrderCustomFieldsRepositoryInterface $orderCustomFieldsRepository)
    {
        $this->orderCustomFieldsRepository = $orderCustomFieldsRepository;
    }

    /**
     * @param Subject $subject
     * @param $result
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterToHtml(Subject $subject, $result): string
    {
        $block = $subject->getLayout()->getBlock('order_custom_fields');

        if ($block !== false) {
            $block->setOrderCustomFields(
                $this->orderCustomFieldsRepository->getOrderCustomFields($subject->getOrder())
            );

            $result = $result . $block->toHtml();
        }

        return $result;
    }
}
