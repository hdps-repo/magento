<?php

namespace LOOP\Checkout\Model\Data;

use Magento\Framework\Model\AbstractExtensibleModel;
use LOOP\Checkout\Api\Data\OrderCustomFieldsInterface;

class OrderCustomFields extends AbstractExtensibleModel implements OrderCustomFieldsInterface
{
    /**
     * @return string|null
     */
    public function getCheckoutPurpose(): ?string
    {
        return $this->getData(self::CHECKOUT_PURPOSE);
    }

    /**
     * @param string|null $checkoutPurpose
     * @return OrderCustomFields|mixed
     */
    public function setCheckoutPurpose(string $checkoutPurpose = null): OrderCustomFields
    {
        return $this->setData(self::CHECKOUT_PURPOSE, $checkoutPurpose);
    }
}
