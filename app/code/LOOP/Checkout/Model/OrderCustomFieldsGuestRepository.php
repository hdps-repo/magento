<?php

namespace LOOP\Checkout\Model;

use Magento\Quote\Model\QuoteIdMaskFactory;
use LOOP\Checkout\Api\OrderCustomFieldsGuestRepositoryInterface;
use LOOP\Checkout\Api\OrderCustomFieldsRepositoryInterface;
use LOOP\Checkout\Api\Data\OrderCustomFieldsInterface;

class OrderCustomFieldsGuestRepository implements OrderCustomFieldsGuestRepositoryInterface
{
    /**
     * @var QuoteIdMaskFactory
     */
    private $quoteIdMaskFactory;

    /**
     * @var OrderCustomFieldsRepositoryInterface
     */
    private $orderCustomFieldsRepository;

    /**
     * @param QuoteIdMaskFactory $quoteIdMaskFactory
     * @param OrderCustomFieldsRepositoryInterface $orderCustomFieldsRepository
     */
    public function __construct(
        QuoteIdMaskFactory $quoteIdMaskFactory,
        OrderCustomFieldsRepositoryInterface $orderCustomFieldsRepository
    ) {
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->orderCustomFieldsRepository = $orderCustomFieldsRepository;
    }

    /**
     * @param string $cartId
     * @param OrderCustomFieldsInterface $customFields
     * @return OrderCustomFieldsInterface
     */
    public function saveOrderCustomFields(
        string $cartId,
        OrderCustomFieldsInterface $customFields
    ): OrderCustomFieldsInterface
    {
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
        return $this->orderCustomFieldsRepository->saveOrderCustomFields((int)$quoteIdMask->getQuoteId(), $customFields);
    }
}
