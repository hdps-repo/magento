<?php

namespace LOOP\Checkout\Model;

use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Sales\Model\Order;
use LOOP\Checkout\Api\OrderCustomFieldsRepositoryInterface;
use LOOP\Checkout\Api\Data\OrderCustomFieldsInterface;

class OrderCustomFieldsRepository implements OrderCustomFieldsRepositoryInterface
{
    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var OrderCustomFieldsInterface
     */
    private $orderCustomFields;

    /**
     * @param CartRepositoryInterface $cartRepository
     * @param ScopeConfigInterface $scopeConfig
     * @param OrderCustomFieldsInterface $orderCustomFields
     */
    public function __construct(
        CartRepositoryInterface $cartRepository,
        ScopeConfigInterface $scopeConfig,
        OrderCustomFieldsInterface $orderCustomFields
    ) {
        $this->cartRepository = $cartRepository;
        $this->scopeConfig = $scopeConfig;
        $this->orderCustomFields = $orderCustomFields;
    }

    /**
     * @param int $cartId
     * @param OrderCustomFieldsInterface $orderCustomFields
     * @return OrderCustomFieldsInterface
     * @throws CouldNotSaveException
     * @throws NoSuchEntityException
     */
    public function saveOrderCustomFields(int $cartId, OrderCustomFieldsInterface $orderCustomFields): OrderCustomFieldsInterface
    {
        $cart = $this->cartRepository->getActive($cartId);

        if (!$cart->getItemsCount()) {
            throw new NoSuchEntityException(__('Cart %1 is empty', $cartId));
        }

        try {
            $cart->setData(
                OrderCustomFieldsInterface::CHECKOUT_PURPOSE,
                $orderCustomFields->getCheckoutPurpose()
            );

            $this->cartRepository->save($cart);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__('Custom order data could not be saved!'));
        }

        return $orderCustomFields;
    }

    /**
     * @param Order $order
     * @return OrderCustomFieldsInterface
     * @throws NoSuchEntityException
     */
    public function getOrderCustomFields(Order $order): OrderCustomFieldsInterface
    {
        if (!$order->getId()) {
            throw new NoSuchEntityException(__('Order %1 does not exist', $order));
        }

        $this->orderCustomFields->setCheckoutPurpose(
            $order->getData(OrderCustomFieldsInterface::CHECKOUT_PURPOSE)
        );

        return $this->orderCustomFields;
    }
}
