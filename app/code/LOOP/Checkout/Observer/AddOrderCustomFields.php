<?php
namespace LOOP\Checkout\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use LOOP\Checkout\Api\Data\OrderCustomFieldsInterface;

class AddOrderCustomFields implements ObserverInterface
{
    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $quote = $observer->getEvent()->getQuote();

        $order->setData(
            OrderCustomFieldsInterface::CHECKOUT_PURPOSE,
            $quote->getData(OrderCustomFieldsInterface::CHECKOUT_PURPOSE)
        );
    }
}
