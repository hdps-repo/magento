<?php

namespace LOOP\Checkout\Block\Order;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order;
use LOOP\Checkout\Api\Data\OrderCustomFieldsInterface;
use LOOP\Checkout\Api\OrderCustomFieldsRepositoryInterface;

class CustomFields extends Template
{
    /**
     * @var Registry|null
     */
    private $coreRegistry = null;

    /**
     * @var OrderCustomFieldsRepositoryInterface
     */
    private $orderCustomFieldsRepository;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param OrderCustomFieldsRepositoryInterface $orderCustomFieldsRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        OrderCustomFieldsRepositoryInterface $orderCustomFieldsRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->coreRegistry = $registry;
        $this->orderCustomFieldsRepository = $orderCustomFieldsRepository;
        $this->_isScopePrivate = true;
        $this->_template = 'order/view/custom-fields.phtml';
    }

    /**
     * @return Order
     */
    public function getOrder() : Order
    {
        return $this->coreRegistry->registry('current_order');
    }

    /**
     * @param Order $order
     * @return mixed
     */
    public function getCustomFields(Order $order)
    {
        return $this->orderCustomFieldsRepository->getOrderCustomFields($order);
    }
}
