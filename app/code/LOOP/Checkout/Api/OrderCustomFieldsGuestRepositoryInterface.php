<?php

namespace LOOP\Checkout\Api;

use Magento\Sales\Model\Order;
use LOOP\Checkout\Api\Data\OrderCustomFieldsInterface;

interface OrderCustomFieldsGuestRepositoryInterface
{
    /**
     * @param string $cartId
     * @param OrderCustomFieldsInterface $customFields
     * @return OrderCustomFieldsInterface
     */
    public function saveOrderCustomFields(string $cartId, OrderCustomFieldsInterface $customFields): OrderCustomFieldsInterface;
}
