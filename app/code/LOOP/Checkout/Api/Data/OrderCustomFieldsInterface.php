<?php

namespace LOOP\Checkout\Api\Data;

interface OrderCustomFieldsInterface
{
    const CHECKOUT_PURPOSE = 'checkout_purpose';

    /**
     * @return string|null
     */
    public function getCheckoutPurpose(): ?string;

    /**
     * @param string|null $checkoutPurpose
     * @return mixed
     */
    public function setCheckoutPurpose(string $checkoutPurpose = null);
}
