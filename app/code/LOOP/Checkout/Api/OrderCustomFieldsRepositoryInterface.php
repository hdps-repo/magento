<?php

namespace LOOP\Checkout\Api;

use Magento\Sales\Model\Order;
use LOOP\Checkout\Api\Data\OrderCustomFieldsInterface;

interface OrderCustomFieldsRepositoryInterface
{
    /**
     * @param int $cartId
     * @param OrderCustomFieldsInterface $customFields
     * @return OrderCustomFieldsInterface
     */
    public function saveOrderCustomFields(int $cartId, OrderCustomFieldsInterface $customFields): OrderCustomFieldsInterface;

    /**
     * @param Order $order
     * @return OrderCustomFieldsInterface
     */
    public function getOrderCustomFields(Order $order) : OrderCustomFieldsInterface;
}
